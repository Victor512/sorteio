package com.victor.sorteio;


import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Random random = new Random();
        Button botao = (Button) findViewById(R.id.button);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int numeroSorteado = 0;

                EditText numeroMin = findViewById(R.id.numeroMin);
                EditText numeroMax = findViewById(R.id.numeroMax);

                String numMin = numeroMin.getText().toString();
                String numMax = numeroMax.getText().toString();

                int nMin = Integer.parseInt(numMin);
                int nMax = Integer.parseInt(numMax);

                numeroSorteado = random.nextInt((nMax - nMin) + 1) + nMin;
                String sorteio = Integer.toString(numeroSorteado);

                TextView resultadoFinal = (TextView) findViewById(R.id.numeroSorteado);

                resultadoFinal.setText(sorteio);

            }
        });
    }
}